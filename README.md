# Assigment-IOCs

This library lets you read data from following data sources:
- https://www.badips.com/get/list/any/2
- http://reputation.alienvault.com/reputation.data
- https://openphish.com/feed.txt


It may support reading data from another pages but it has not been tested yet.

Data are stored in 3 tables:
- The first table contains ip addressess with id. These ips are unique
- The second table contains urls with id. These urls are unique
- The third table contains ips and urls with ids and source of data.


**Requirments:**\
Python 3 (or higher)\
psycopg2 library for python\
postgresql server\


Usage: ioc.py [-h] [-i I [I ...]] [--create]

optional arguments:\
  -h, --help    show this help message and exit\
  -i I [I ...]  List of internet data sources to process\
  --create      If true then create empty tables in database (default=False)\
 
Example:\
python ioc.py --create -i "https://openphish.com/feed.txt" "https://www.badips.com/get/list/any/2" "http://reputation.alienvault.com/reputation.data"
 
 
  
  


