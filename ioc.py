import sys
import argparse
import logging
import re
from urllib.request import urlopen
from urllib.error import URLError
from urllib.parse import urlparse
import database
from psycopg2 import DatabaseError


def process_data(iurl, idb):
    try:
        file = urlopen(iurl)
    except URLError:
        logging.error('An error occurred while opening an url')
        return
    url_source = urlparse(iurl).netloc
    lines = file.readlines()

    gen_lines = (process_line(line) for line in lines if process_line(line) is not None)
    ips = []
    urls = []
    for i in gen_lines:
        if i['ip']:
            ips.append((i['ip'],))
        elif i['url']:
            urls.append((i['url'],))
        else:
            print(i)

    idb.insert_ips(ips, url_source)
    idb.insert_urls(urls, url_source)


def process_line(iline):
    iline = iline.decode('utf-8').strip()
    urls = re.findall(r'\b(ftp|http|https):\/\/[^ "]+\b', iline)
    ips = re.findall(r'\b\d{1,3}\.\d{1,3}\.\d{1,3}.\d{1,3}\b', iline)

    out = {"url": "", "ip": ""}
    if urls:
        out["url"] = iline
    elif ips:
        out["ip"] = ips[0]
    else:
        return None

    return out


def main(iurls, icreate=False):
    try:
        db = database.Database()
    except DatabaseError:
        logging.error('An error occurred while opening a database')
        sys.exit(1)

    if icreate:
        db.create_tables()

    for url in iurls:
        process_data(url, db)

    db.close()


if __name__ == "__main__":
    logging.basicConfig(level=logging.ERROR)
    parser = argparse.ArgumentParser()
    parser.add_argument('-i', nargs='+', type=str,
                        help="The list of internet data sources to process")
    parser.add_argument('--create', default=False, action='store_true',
                       help="If true then create empty tables in database (default=False)")
    args = parser.parse_args()

    main(args.i, args.create)


