import psycopg2
from psycopg2 import extras
from configparser import ConfigParser


class Database():
    def __init__(self):
        self.config_file = "database_config.ini"
        self.params = self.config()
        self.conn = psycopg2.connect(**self.params)
        self.cur = self.conn.cursor()
        self.conn.autocommit = True

    def config(self):
        """ load config data from config file """
        parser = ConfigParser()
        parser.read(self.config_file)
        section = "postgresql"
        con = {}
        if parser.has_section(section):
            params = parser.items(section)
            for param in params:
                con[param[0]] = param[1]
        else:
            raise Exception(f"Section {section} not found in the {self.config_file}")

        return con

    def create_tables(self):
        """ create tables for ip_adresses, sourcem urls in the PostgreSQL database"""
        command1 = """
        DROP TABLE IF EXISTS ip_addresses;
        CREATE TABLE ip_addresses (
        id SERIAL PRIMARY KEY,
        ip_address INET UNIQUE NOT NULL )"""
        command2 = """
        DROP TABLE IF EXISTS source;
        CREATE TABLE source (
        id INTEGER NOT NULL,
        type VARCHAR(3) NOT NULL,
        src VARCHAR NOT NULL)"""
        command3 = """
        DROP TABLE IF EXISTS urls;
        CREATE TABLE urls (
        id SERIAL PRIMARY KEY,
        url VARCHAR UNIQUE NOT NULL)"""

        conn = None
        self.cur.execute(command1)
        self.cur.execute(command2)
        self.cur.execute(command3)
        self.conn.commit()

    def insert_ips(self, ips, iurl_source):
        command = f"""with rows as (
            INSERT INTO ip_addresses (ip_address) VALUES %s
            ON CONFLICT ON CONSTRAINT ip_addresses_ip_address_key
            DO NOTHING RETURNING id, ip_address)
            INSERT INTO source (id, type, src)
            SELECT id, 'ip', '{iurl_source}'
            FROM rows;"""
        extras.execute_values(self.cur, command, ips)

    def insert_urls(self, iurls, iurl_source):
        command = f"""with rows as (
        INSERT INTO urls (url) VALUES %s
        ON CONFLICT ON CONSTRAINT urls_url_key
        DO NOTHING RETURNING id, url)
        INSERT INTO source (id, type, src)
        SELECT id, 'url', '{iurl_source}'
        FROM rows;"""
        extras.execute_values(self.cur, command, iurls)

    def close(self):
        if self.cur is not None:
            self.cur.close()
        if self.conn is not None:
            self.conn.close()